# LivePose - website - documentation

This is the repository for building LivePose docs website.

## License

This documentation is licensed under a [Creative Commons Attribution Non-Commercial ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-nc-sa/4.0/)

All the source code (including examples and excerpt from the software) is licensed under the [GNU Public License version 3 (GPLv3)](https://www.gnu.org/licenses/gpl-3.0.en.html)

## Code of Conduct

By participating in this project, you agree to abide by the [Code of Conduct](CODE_OF_CONDUCT.md). We expect all contributors to follow the [Code of Conduct](CODE_OF_CONDUCT.md) and to treat fellow humans with respect. It must be followed in all your interactions with the project.

## Installation

[[_TOC_]]

## get started: install sphinx

First, clone this repository and open a terminal in its root folder.

Create a Python virtualenv:

    python3 -m venv venv

Activate the virtualenv:

    source venv/bin/activate
    
`venv/*` is listed in the `.gitignore` file so that this folder is not tracked by `git`.

Install Sphinx:

    pip install -U Sphinx

## update project information

**Not recommended**: overwrite the template configuration by using the `sphinx-quickstart` command:

    rm source/conf.py source/index.rst
    sphinx-quickstart --sep -p"LivePose" -a"metalab" -v"0.0.1" -r"0.0.1" -l"en"

**Recommended**: update [source/conf.py](source/conf.py) with project-specific information:
```py
project = 'LivePose'
copyright = '2021, Metalab'
author = 'Metalab'

# The short X.Y version
version = '0.0.1'

# The full version, including alpha/beta/rc tags
release = '0.0.1'
```

`build/*`, `*.bat` are not tracked by git as they are listed in .gitignore.

## first checkpoint: make a commit

Let's make a git commit to keep track of the progress made.

## add sphinx-intl

Install `sphinx-intl` to support multiple locales for the website:

    pip install wheel
    pip install sphinx-intl

Locales are enabled by the following configuration in the `source/conf.py` file:

    locale_dirs = ['locale/']   # path is example but recommended.
    gettext_compact = False     # optional.

Extract messages from the `.rst` files that were created by `sphinx-quickstart`:

    make gettext

Generated `*.mo` files are ignored, being listed in `.gitignore`.

Generate the `.po` files associated with the `.rst` files. The `-l` option allows us to select the locale:

    sphinx-intl update -p build/gettext -l en
    sphinx-intl update -p build/gettext -l fr

*Optional: In the current project, we translated one message from En->Fr as a way to keep track of versions and to double-check that everything is working fine. You don't have to do this for now.*

## second checkpoint

Let's make a git commit!

## install Furo theme

We now want to personalize the website with the Furo theme.

First, install the theme inside the virtualenv:

    pip install furo

The furo theme is enabled in `source/conf.py` by:

    html_theme = "furo"

## checkpoint 3

Let's make a git commit!

## add the landing page

### style

We are now ready to further customize the docs so that it fits what is currently in use at the Metalab. All the files mentioned are available in this repo.

* `source/_static/css/custom_landing_page.css` (optional: update to change the style)

* `source/_templates/furo_page.html`

* `source/_templates/index.html`

* `source/_templates/page.html`

* `source/index.html`

* `source/index.rst`

All the previously mentioned files are `source/conf.py` through this configuration:
    
    def setup(app):
        app.add_css_file('css/custom_landing_page.css')


We will now generate the `.pot` files associated with the landing page:

    make gettext

The `.pot` files will be used to generate `.po` files, used as the base for localization:

    sphinx-intl update -p build/gettext -l fr

### contents

The name of the page that will be used to create the `index.html` for the landing page is configured in `source/conf.py` by:

     master_doc = 'contents'

It corresponds to file:

* `source/contents.md `

Now you should be able to `sphinx-build` your website! Let's try it:

    sphinx-build -b html -D language=en source public/en
    sphinx-build -b html -D language=fr source public/fr
    
The files are ready to be viewed in a web browser. You may start a `localhost` server with Python:

    python3 -m http.server

### checkpoint 4

Let's commit our changes.

## restrict the custom CSS to the landing page

you might need to tweak the css tag names and to update the index.rst sections to prevent the css from the landing page to leak onto the docs... will make an issue for this - I managed to make it work in SATIE... TODO

## update the CI

We will update the CI so that we can deploy the website to Gitlab Pages:

    pip freeze > requirements.txt

We will *update the Makefile* so that it contains the commands used by the Gitlab CI. You can copy the `Makefile` in this repo into yours.

Add .gitlab-ci.yml to the root of `livepose`, so that Gitlab will be able to find it.

Add public/* to .gitignore - this is were the newly available `make doc` and `make intl` commands are.

You can now make the docs with your local changes by using `make intl` and `make doc`. If you are happy with the result, you can push them to Gitlab to see your website live - it may take a few minutes.


## checkpoint 5

We are done for now - let's commit this work :)

# further steps

we would need to update this template to make it able to switch between versions and locales at anytime in the docs.

let's start with the locales menu

## locales menu

### initialize the locale menu

inspiration from https://stackoverflow.com/a/63054018

_in `source/_templates/`, create a `sidebar` folder and add the `navigation.html` file to it._

_in `source/conf.py`, uncomment these three lines at the beginning of the file:

```python

import os
import sys
sys.path.insert(0, os.path.abspath('.'))

```

_in `source/conf.py`, add the following lines:_

```python

# -- LOCALE MENU -------------------------------------------------------------

# POPULATE LINKS TO OTHER LANGUAGES

try:
   html_context
except NameError:
   html_context = dict()

# SET CURRENT_LANGUAGE
if 'current_language' in os.environ:
   # get the current_language env var set by buildDocs.sh
   current_language = os.environ['current_language']
else:
   # the user is probably doing `make html`
   # set this build's current language to english
   current_language = 'en'
 
# tell the theme which language to we're currently building
html_context['current_language'] = current_language

html_context['languages'] = []
html_context['languages'].append( ('en', '/documentations/livepose/' + 'en' + '/') )
 
languages = [lang.name for lang in os.scandir('locale') if lang.is_dir()]
for lang in languages:
   html_context['languages'].append( (lang, '/documentations/livepose/' + lang + '/') )

```

try to build the docs with:

    make intl

    make doc

you should now have the files in the `public/` folder, with the locale version menu in the leftbar menu.

### update the locale menu to the new locale menu version

after having initialized the locale menu, we are going to make a few changes so that we can build the locale menu in differents environments.

this is based on [MR 51 in Splash repo](https://gitlab.com/sat-metalab/documentations/splash/-/merge_requests/51).

* in `source/_templates/sidebar/navigation.html`, we want to add a baseurl variable to the line defining the slug.

the complete end result should be:

```
<div class="sidebar-tree">
  {{ furo_navigation_tree }}
</div>


{% block menu %}

<div class="rst-versions" data-toggle="rst-versions" role="note" aria-label="versions">

    <div class="rst-other-versions">
    {% if languages|length >= 1 %}
    <dl>
        <dt>{{ _('Languages') }}</dt>
            {% for slug, url in languages %}
                <dd><a href="{{ baseurl ~ url ~ pagename ~ '.html'}}">{{ slug }}</a></dd>
            {% endfor %} 
    </dl>
    {% endif %}

    </div>
</div>

{% endblock %}
```
* in `source/conf.py`, we will delete the `documentations/livepose` lines:

```
# tell the theme which language to we're currently building
html_context['current_language'] = current_language

html_context['languages'] = []
html_context['languages'].append( ('en', 'en/') )
 
languages = [lang.name for lang in os.scandir('locale') if lang.is_dir()]
for lang in languages:
   html_context['languages'].append( (lang, lang + '/') )
```
* in Makefile, we are going to define new targets:

```
htmlgitlabdocs:
	@$(SPHINXBUILD) -b html -D language=en -A baseurl='/./documentations/livepose/' source public/en
	@$(SPHINXBUILD) -b html -D language=fr -A baseurl='/./documentations/livepose/' source public/fr
	cp source/index.html public/
	
htmllocaltest:
	@$(SPHINXBUILD) -b html -D language=en -A baseurl='/./public/' source public/en
	@$(SPHINXBUILD) -b html -D language=fr -A baseurl='/./public/' source public/fr
	cp source/index.html public/

htmlgitlab:
	@$(SPHINXBUILD) -b html -D language=en -A baseurl='/./livepose/' source public/en
	@$(SPHINXBUILD) -b html -D language=fr -A baseurl='/./livepose/' source public/fr
	cp source/index.html public/
```

* in `gitlab-ci.yml`, we are going to use the target with the appropriate baseurl for deploying on gitlab. the end result should be:

```
pages:
  stage: deploy
  image: python:3.8
  script:
  - pip install -U sphinx
  - pip install -r requirements.txt
  - make htmlgitlabdocs
  artifacts:
    paths:
    - public
  only:
  - main
```

TODO: tweak CSS so that it fits in with the Furo theme.



