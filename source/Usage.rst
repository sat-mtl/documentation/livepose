Usage
=====

Running LivePose
----------------

LivePose is a command-line tool working with a configuration file, and
as such is meant to be invoked from a terminal. Once installed, you can
get a list of all options by doing:

.. code:: bash

   livepose --help

A default configuration file is provided in the ``config`` folder
located at the root of the repository.

For example, to run LivePose to send all skeletons data detected on the
first Video4Linux camera to a remote host which IP address is
192.168.2.144, via OSC, create the following config:

::

   {
       "backend": {
           "name": "posenet",
           "params": {
               "device_id": 0
           }
       },
       "cameras": {
           "input_paths":["/dev/video0"],
           "camera_matrices": {}
       },
       "filters": {
           "skeletons": {}
       },
       "outputs": {
           "osc": {
               "destinations": {
                   "192.168.2.144": 9000
               }
           }
       }
   }

Save it to file, and run from the terminal:

.. code:: bash

   livepose --config /path/to/the/config/file.json

After a short time, a window showing the camera should be displayed with
the detected skeletons overlayed, and OSC messages should be received on
port 9000 of the remote host.
