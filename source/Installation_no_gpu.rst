Installing LivePose Without GPU Support
=======================================

These are instructions for installing LivePose without GPU support.

Note that the framerate may be considerably lower without GPU acceleration.

Clone The LivePose Repo
-----------------------

1. Clone the LivePose repo

.. code:: bash

   sudo apt install git
   git clone https://gitlab.com/sat-mtl/tools/livepose
   cd livepose

2. Install and setup Git LFS

.. code:: bash

   sudo apt install git-lfs
   git lfs fetch && git lfs pull

Install Dependencies
--------------------

Install Main Dependencies with apt
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Install all system dependencies

.. code:: bash

   sudo apt install software-properties-common
   sudo add-apt-repository ppa:sat-metalab/metalab
   sudo apt update
   sudo apt install build-essential python3-dev python3-pip python3-venv python3-wheel
   sudo apt install libavresample-dev libavcodec-dev libavformat-dev libavutil-dev libswscale-dev
   sudo apt install libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev
   sudo apt install libgtk2.0-dev libgtk-3-dev
   sudo apt install python3-libmapper v4l-utils

Create a Virtual Environment
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

LivePose has Python dependencies installed through apt and pip. We create a virtual environment so that LivePose can access dependencies from both sources.

1. Create a virtual environment (you can name this whatever you like)

.. code:: bash

   python3 -m venv --system-site-packages --symlinks ~/livepose_venv

2. Activate the virtual environment

.. code:: bash

   source ~/livepose_venv/bin/activate

3. Install remaining Python dependencies with pip

.. code:: bash

   export SETUPTOOLS_USE_DISTUTILS=stdlib # ModuleNotFoundError: No module named 'setuptools._distutils'
   LIVEPOSE_EXTRAS="posenet;osc" pip3 install .

Note: optional backends and outputs can be activated as a semicolon-separated list with environment variable ``LIVEPOSE_EXTRAS``. Examples:

-  ``pip3 install .``: if ``LIVEPOSE_EXTRAS`` is not defined, by default all extras are activated their dependencies searched and required
-  ``LIVEPOSE_EXTRAS="all" pip3 install .``: activates all extras and installs their dependencies (useful if ``LIVEPOSE_EXTRAS`` had already been set to other values)
-  ``LIVEPOSE_EXTRAS="posenet;osc" pip3 install .``: activates and searches dependencies for ``posenet`` and ``osc``, but deactivates all other extras (such as ``libmapper``, ...)

Tip: use option ``-e`` in development mode, so that ``pip3 install -e .`` installs all dependencies but LivePose itself, and thus LivePose source code modifications are active directly at runtime without re-installation.

Run the Demo
------------

You can now run LivePose! To try it out with the default settings run the following command from the top- level ``livepose/`` directory:

.. code:: bash

   ./livepose.sh -c livepose/configs/default.json

To exit the virtual environment when you are not installing or using LivePose:

.. code:: bash

   deactivate
